//
//  YFAsanaTypeReusableView.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAsanaTypeReusableView.h"

@implementation YFAsanaTypeReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setData:(NSString *)data
{
    _data = data;
    self.asanasType_lbl.text = data;
}
@end
