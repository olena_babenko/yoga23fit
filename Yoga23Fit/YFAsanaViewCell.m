//
//  YFAsanaViewCell.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAsanaViewCell.h"

@implementation YFAsanaViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setData:(YFAsana *)data
{
    _data = data;
    self.asanaName_lbl.text = data.name;
    
}

@end
