//
//  YFDataViewController+CheckForUpdate.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController.h"

@interface YFDataViewController (CheckForUpdate)

-(void)checkForUpdateforLocalPath:(NSString *)localPath orRemotePath:(NSURL *)remotePath;

-(NSArray *)localDataRequestArrayfromFileDict:(NSDictionary *)dictRoot;
@end
