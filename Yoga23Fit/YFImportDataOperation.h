//
//  YFImportDataOperation.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/26/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YFStorage;
@interface YFImportDataOperation : NSOperation

@property (nonatomic, copy) void (^progressCallback) (float);

//expected array of dictionaries
-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andData:(NSArray *)dataToParse;




@end
