//
//  YFGetDataOperation.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/27/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YFStorage;
@interface YFGetDataOperation : NSOperation

@property (nonatomic, copy) void (^completeCallback) (NSArray *objects, NSError *error);

-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andPredicate:(NSPredicate*)predicate;
@end
