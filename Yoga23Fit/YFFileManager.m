//
//  YFFileManager.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFFileManager.h"


@implementation YFFileManager

+(NSFileManager *)fileManager
{
    return [NSFileManager defaultManager];
}

+(NSString *)rootFolderWithName:(NSString *)folderName withParentPath:(NSString *)parentFolder
{
    NSFileManager *currentFileManager = [self fileManager];
     NSError *error;
    NSString *pathAppendix = [NSString stringWithFormat:@"/%@", folderName];
    NSString *dataPath=  [parentFolder stringByAppendingPathComponent:pathAppendix];
    if (![currentFileManager fileExistsAtPath:dataPath])
        [[self fileManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
    return dataPath;

}
+(NSString *)rootFolderWithName:(NSString *)folderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [self rootFolderWithName:folderName withParentPath:documentsDirectory];
}
+(NSString *)catalogFolderPath
{
    return [self rootFolderWithName:@"Catalog"];
}
+(NSString *)programFolderPath
{
    return [self rootFolderWithName:@"Program"];
}


+(NSURL *)pathForFolderCatalogObject:(NSString *)folderName
{
    NSString *rootFolderName = [self catalogFolderPath];
    NSString *folderPath=  [self rootFolderWithName:folderName withParentPath:rootFolderName];
    return [NSURL fileURLWithPath:folderPath];
}

+(NSURL *)pathForFolderProgramObject:(NSString *)folderName
{
    NSString *rootFolderName = [self programFolderPath];
    NSString *folderPath=  [self rootFolderWithName:folderName withParentPath:rootFolderName];
    return [NSURL fileURLWithPath:folderPath];
}

+(BOOL)removeFileOrFolderFromURL:(NSURL *)url
{
    NSFileManager *fileManager = [self fileManager];
    // NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:[url path]];
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:[url path] error:nil];
        if (!success) return NO;
        else    return YES;
    }
    return YES;
}

+(BOOL)isExistFileOrFolder:(NSURL *)url
{
    NSFileManager *fileManager = [self fileManager];
    // NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:[url path]];
    return fileExists;
}


@end
