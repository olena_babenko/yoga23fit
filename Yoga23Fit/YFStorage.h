//
//  YFData.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YFStorage : NSObject

@property (nonatomic,strong,readonly) NSManagedObjectContext* mainManagedObjectContext;

- (void)saveContext;
//Context for background thread
- (NSManagedObjectContext*)privateContext;

//загрузка - в background контексте (много асан)
//изменение (отдельной асаны) в главном контексте (одна асана)

- (NSManagedObjectModel *)managedObjectModel;
@end
