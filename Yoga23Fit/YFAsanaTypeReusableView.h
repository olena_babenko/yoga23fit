//
//  YFAsanaTypeReusableView.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YFAsanaTypeReusableView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *asanasType_lbl;

@property (weak, nonatomic) NSString *data;
@end
