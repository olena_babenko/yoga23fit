//
//  YFAsanasCollectionViewController.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAsanasCollectionViewController.h"
#import "YFDataViewController_Parent.h"

#import "YFFetchedResultCollectionDataSource.h"
#import "YFAsana.h"
//for filering data
#import "NSPredicate+Filters.h"
//for dispaying cells

#import "YFAsanaTypeReusableView.h"
#import "YFAsanaViewCell.h"

#import "YFDataViewController+CheckForUpdate.h"
#import "NSArray+checngeValueInAllObjects.h"

@interface YFAsanasCollectionViewController ()<YFFetchedResultCollectionDataSourceDelegate>

@property (assign, nonatomic) YFAsanaTypes currentAsanasType;

@end

static NSString* cellsReuseIdentifier=@"YFAsanaViewCell";
static NSString* cellsReuseIdentifierHeader=@"YFAsanaTypeReusableView";

@implementation YFAsanasCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setStorage:(YFStorage *)storage
{
    [super setStorage:storage];
}

-(NSString *)entityName
{
    return @"YFAsana";
}
-(NSArray *)sortDescriptors
{
    return @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
}
-(NSString *)sectionNamePath
{
    return @"type";
}

//change type
-(void)setCurrentAsanasType:(YFAsanaTypes)currentAsanasType
{
    if (_currentAsanasType != currentAsanasType) {
        _currentAsanasType = currentAsanasType;

        NSString *type_value = [YFAsana stringFromType:_currentAsanasType];
        self.dataFilter = [NSPredicate asanaTypeFilter:type_value];
//lets check for update
        NSString *pathForLocalResource = [self pathForLocalResource];
//TODO: replace for valid URL for asana
        NSURL *pathForRemoteresource = [NSURL URLWithString:@"https://api.github.com/users/mralexgray/repos"];
//TODO: update after complete
        [self checkForUpdateforLocalPath:pathForLocalResource orRemotePath:pathForRemoteresource];
//lets change predicate first
        
    }
}
-(NSString *)pathForLocalResource
{   switch (_currentAsanasType) {
        case YFAsanaType1:
            return @"AsansType1";
        case YFAsanaType2:
            return @"AsansType2";
        case YFAsanaType3:
            return @"AsansType3";
    default:
        break;
    }
    NSAssert(NO, @"No local file for resource type %@", [YFAsana stringFromType:_currentAsanasType]);
    return nil;
}

- (void)viewDidLoad
{
    self.state = YFCollectionViewState;
    [self.collectionView registerNib:[UINib nibWithNibName:@"YFAsanaViewCell" bundle:nil]  forCellWithReuseIdentifier:cellsReuseIdentifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"YFAsanaTypeReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellsReuseIdentifierHeader];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.currentAsanasType = YFAsanaType1;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Data Source Delegate

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    YFAsanaViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:cellsReuseIdentifier forIndexPath:indexPath];
    cell.data = (YFAsana *)item;
    return cell;
    
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath withInfo:(NSString *)info
{
    YFAsanaTypeReusableView *header = [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:cellsReuseIdentifierHeader forIndexPath:indexPath];
    header.data = info;
    return header;
}

#pragma mark - Search

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSPredicate* predicate = [NSPredicate asanaNumeNumberFilterForSearchText:searchText];
    [self.dataSource changePredicate:predicate];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

#pragma mark - Страшная штука которую нужно переделать
//пересено полностью из TAble View

- (IBAction)tab_changed:(id)sender {
    
    UISegmentedControl *segment = (UISegmentedControl *)sender;
    
   // NSDictionary *dictRoot;
    //NSString *path_forResource;
    if (segment.selectedSegmentIndex == 0) {
        self.currentAsanasType = YFAsanaType1;
    }
    if (segment.selectedSegmentIndex == 1) {
        self.currentAsanasType = YFAsanaType2;
    }
    
   
}

//method from CHECK FOR UPDATE Category, to get Right Array from Local File

-(NSArray *)localDataRequestArrayfromFileDict:(NSDictionary *)dictRoot
{
    NSString *type_value = [YFAsana stringFromType:_currentAsanasType];
    NSArray *arrayList = [NSArray arrayWithArray:[dictRoot objectForKey:@"asanas"]];
    arrayList = [arrayList foreachSetValue:type_value forKey:@"type"];
    return arrayList;
}

@end
