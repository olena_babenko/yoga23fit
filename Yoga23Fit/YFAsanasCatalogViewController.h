//
//  YFAsanasCatalogViewController.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController.h"

@interface YFAsanasCatalogViewController : YFDataViewController<UITableViewDelegate, UISearchBarDelegate>


- (IBAction)tab_changed:(id)sender;

@end
