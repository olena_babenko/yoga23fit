//
//  main.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YFAppDelegate class]));
    }
}
