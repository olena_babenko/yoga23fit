//
//  YFAppDelegate.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
