//
//  YFVersionsCheckOperation.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/5/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YFStorage;
@interface YFVersionsCheckOperation : NSOperation

@property (nonatomic, copy) void (^completeCallback) (NSArray *objects, NSError *error);

-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andUpdateURL:(NSURL *)updateURL;

@end
