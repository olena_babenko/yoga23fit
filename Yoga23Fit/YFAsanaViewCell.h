//
//  YFAsanaViewCell.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFAsana.h"

@interface YFAsanaViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *asanaName_lbl;

@property (weak, nonatomic) YFAsana *data;

@end
