//
//  YFDataViewController+CheckForUpdate.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController+CheckForUpdate.h"
#import "YFDataViewController_Parent.h"

#import "YFImportDataOperation.h"

#import "YFVersionsCheckOperation.h"

@implementation YFDataViewController (CheckForUpdate)

-(void)checkForUpdateforLocalPath:(NSString *)localPath orRemotePath:(NSURL *)remotePath
{
    if ([self.dataSource isEmpty]&&[self shouldCheckLocalForUpdate:localPath]) {
        // try to load from the local storage
        NSLog(@"Empty");
        [self updateLocalDataWithPath:localPath];
    }else
    {
        //update
        NSLog(@"Not Empty");
        if ([self shouldCheckRemoteForUpdate:remotePath]) {
         
            [self updateRemoteDataWithURL:remotePath];
        }
    }
    
}


#pragma mark - REMOTE STORAGE Update

//to load from remote storage, only after some time
-(BOOL)shouldCheckRemoteForUpdate:(NSURL *)remotePath
{
    
    NSString *remoteKey = [self remoteURLKey:remotePath];
    
    // get last update Date
    NSDate *lastUpdate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:remoteKey];
    if (lastUpdate) {
        NSLog(@"We have last Update %@ from Remote", lastUpdate);
        NSInteger difference = ((NSInteger)round([lastUpdate timeIntervalSinceNow]));
        NSInteger secondForUpdate = [self secondsForUpdateForPath:remotePath];
        if (difference > secondForUpdate) {
            NSLog(@"OUT DATE DATA! Lets Refresh");
            return YES;
        }
        return NO;
    }else
    //Only while debbugiong
        return NO;
    // return YES; <- normal Value
    
}
-(NSString *)remoteURLKey:(NSURL *)remotePath
{
    // CAN BE SOME ISSUES if NSURL include smth exept path (then we should implement additional method
    //  to get unique key
    return  [remotePath absoluteString];
}
-(void)updateRemoteDataWithURL:(NSURL *)remotePath
{
 //TODO: make the right one URL
    YFVersionsCheckOperation *version_check = [[YFVersionsCheckOperation alloc] initWithStorage:self.storage entity:self.entityName andUpdateURL:remotePath];
    __weak typeof(self) weakSelf = self;
    version_check.completeCallback = ^(NSArray *objects, NSError *error){
        if (error.code == noErr) {
            // Lets Import(update) results
            [weakSelf importWithObjects:objects];
//we save Data only if loading was successful but it can be changed
            [weakSelf saveRemoteLastUpdateForURL:remotePath];
        }
    };
    [self.operationQueue addOperation:version_check];
}
-(void)importWithObjects:(NSArray *)objects
{
    YFImportDataOperation *import = [[YFImportDataOperation alloc] initWithStorage:self.storage entity:self.entityName andData:objects];
     [self.operationQueue addOperation: import];
}

-(void)saveRemoteLastUpdateForURL:(NSURL *)remotePath
{
     NSString *remoteKey = [self remoteURLKey:remotePath];
    NSDate *currentDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:currentDate forKey:remoteKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


#pragma mark - LOCAL STORAGE Update

//to load from local storage only once!
-(BOOL)shouldCheckLocalForUpdate:(NSString *)localPath
{
     BOOL is_already_loaded =  ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:localPath]).boolValue;
//if we loaded data from file -> we shouldnt reload data;
    return !is_already_loaded;
}

-(void)updateLocalDataWithPath:(NSString *)localPath
{
    NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:localPath ofType:@"plist"]];
    
    NSArray *arrayList = [self localDataRequestArrayfromFileDict:dictRoot];
    
    YFImportDataOperation *import_op = [[YFImportDataOperation alloc] initWithStorage:self.storage entity:[self entityName] andData:arrayList];
    [self.operationQueue addOperation:import_op];

        
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:localPath];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(NSArray *)localDataRequestArrayfromFileDict:(NSDictionary *)dictRoot
{
    return nil;
}
@end
