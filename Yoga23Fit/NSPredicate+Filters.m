//
//  NSPredicate+Filters.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "NSPredicate+Filters.h"


@implementation NSPredicate (Filters)

+(NSPredicate *)findValue:(id)value forKey:(NSString *)key checkClass:(NSString *)class_name
{
   // NSAssert([self if_ExistProperty:key inClassName:class_name], @"[NSPredicate+Filter]: Don't exist property %@ in class %@",key, class_name);
    
    return [NSPredicate predicateWithFormat:@"%K == %@",key,value];
}
+(NSPredicate *)findIncludeValue:(id)value forKey:(NSString *)key checkClass:(NSString *)class_name
{
    //NSAssert([self if_ExistProperty:key inClassName:class_name], @"[NSPredicate+Filter]: Don't exist property %@ in class %@",key, class_name);
    
    return [NSPredicate predicateWithFormat:@"%K contains[cd] %@",key, value];
}

+(NSPredicate *)activityNameInclude:(NSString *)name_search
{
    return [self findIncludeValue:name_search forKey:@"name" checkClass:@"YFActivity"];
}
+(NSPredicate *)programNameInclude:(NSString *)name_search
{
    return [self findIncludeValue:name_search forKey:@"name" checkClass:@"YFProgram"];
}
//TODO: Поиск должен осуществляться только по асанам is_complete (у которой загружены все обязательные данные) но это в идеале. Пока ищем только по одному полю
+(NSPredicate *)asanaTypeFilter:(NSString *)typeName
{
    return [self findValue:typeName forKey:@"type" checkClass:@"YFAsana"];
}
+(NSPredicate *)asanaFamilyFilter:(NSString *)familyName
{
    return [self findValue:familyName forKey:@"family" checkClass:@"YFAsana"];
}
+(NSPredicate *)asanaNameIncludeFilter:(NSString *)name
{
    return [self findIncludeValue:name forKey:@"name" checkClass:@"YFAsana"];
}
+(NSPredicate *)asanaNumberFilter:(NSNumber *)number
{
    return [self findValue:number forKey:@"catalogID" checkClass:@"YFAsana"];
}
+(NSPredicate *)asanaNumeNumberFilterForSearchText:(NSString*) searchText{
    static NSNumberFormatter* formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.maximumFractionDigits = 0;
    });
    
    NSPredicate* predicate = nil;
    
    if(searchText.length){
        NSNumber* number = [formatter numberFromString: searchText];
        if(nil!=number){
            predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[
                                                                            [self asanaNumberFilter:number],
                                                                            [self asanaNameIncludeFilter:searchText]
                                                                            ]];
        }else{
            predicate = [NSPredicate asanaNameIncludeFilter:searchText];
        }
    }
    return predicate;
}

//check if we accedentely change property name
+(BOOL)if_ExistProperty:(NSString *)property inClassName:(NSString *)class_name
{
    //NO -> Fail
    //YES ->Continue
    Class check_class = NSClassFromString (class_name);
    if([check_class isSubclassOfClass:[NSManagedObject class]]){
        
#warning TODO: using [instancesRespondToSelector: for nested properties returns NO, so temporarily just returning true. Implement later
        
        return YES;
    }else{
        return [check_class instancesRespondToSelector:NSSelectorFromString(property)];
    }
}
@end
