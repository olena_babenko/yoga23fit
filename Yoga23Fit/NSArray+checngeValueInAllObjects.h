//
//  NSArray+checngeValueInAllObjects.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (checngeValueInAllObjects)

-(NSArray *)foreachSetValue:(id)value forKey:(NSString *)key;
@end
