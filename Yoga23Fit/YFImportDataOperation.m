//
//  YFImportDataOperation.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/26/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFImportDataOperation.h"
#import "YFStorage.h"
#import "YFParsing.h"

static const int ImportBatchSize = 10;

@interface YFImportDataOperation ()

@property (strong, nonatomic) NSArray *dataToParse;
@property (strong, nonatomic) YFStorage *storage;
@property (strong, nonatomic) NSString *entityName;
@property (nonatomic, strong) NSManagedObjectContext* context;

@end

@implementation YFImportDataOperation

-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andData:(NSArray*)dataToParse
{
    self = [super init];
    if(self) {
        self.storage = storage;
        self.entityName = entityName;
        self.dataToParse = dataToParse;
    }
    return self;
}
-(void)main
{
    self.context = [self.storage privateContext];
    self.context.undoManager = nil;
    [self.context performBlockAndWait:^(void)
    {
        [self import];
    }];
    
}//All of these compicatnessness for not Search and replace Every object SEPARATELY
//and have an ability to CANCEL Parsing
-(void)import
{
    
    //1 get all IDs
        //create entity class
        Class<YFParsing> entity_class = NSClassFromString(self.entityName);
        //find ids only (to find them in DB)
        NSArray *ids_only = [self arrayIDOnlyFromEntity:entity_class];
        //id field name in DB
        NSString *propertyId = [entity_class propertyID];
        //check if we have such ID
        //NSAssert([entity_class instancesRespondToSelector:NSSelectorFromString(propertyId)], @"[YFImportDataOperation import]: Class %@ does not have an ID:%@", entity_class, propertyId);
    
    //2 request this IDs from database
    NSMutableArray *matchingDBObjects = [[self matchingObjectsWithIds:ids_only forField:propertyId] mutableCopy];
    
    NSInteger count = self.dataToParse.count;
    NSInteger progressGranularity = count/100;

    [self.dataToParse enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    
        //cancelation process
        //stop parsing on cancel
        if(self.isCancelled) {
            *stop = YES;
            return;
        }
        NSString *entity_id =[entity_class idFromData:obj];
        //find object from matchingArray
        NSManagedObject<YFParsing> *obj_to_update = [[matchingDBObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(%K IN %@)",propertyId, entity_id]] firstObject];
    //3 if exist - update
        if (obj_to_update) {
            [obj_to_update updateMOWithData:obj];
            //lets not search it anymore
            [matchingDBObjects removeObject:obj_to_update];
        }else{
    //4 if not exist - create
           obj_to_update = (NSManagedObject<YFParsing> *)[entity_class createMOWithData:obj inContex:self.context];
        }
        if (self.progressCallback) {
            if (idx % progressGranularity == 0) {
                self.progressCallback(idx / (float) count);
            }
            
        }
    //5 save context
        //save only every ImportBatchSize objects
        if (idx % ImportBatchSize == 0) {
            [self.context save:NULL];
        }
    }];
    //final save for the rest of object (Example: we have less objects then ImportBatchSize)
    [self.context save:NULL];
    
}
-(NSArray *)arrayIDOnlyFromEntity:(Class<YFParsing>) entity_class
{
    NSMutableArray *ids_only = [[NSMutableArray alloc] init];
    [self.dataToParse enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        
        if(self.isCancelled) {
            *stop = YES;
            return;
        }
        [ids_only addObject:[entity_class idFromData:obj]];
    }];
    return [ids_only copy];
}
-(NSArray *)matchingObjectsWithIds:(NSArray *)ids forField:(NSString *)propertyId
{
    if(self.isCancelled) {
        return nil;
    }
    //2 create the fetch request to get all Employees matching the IDs.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:self.entityName inManagedObjectContext:self.context]];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat:@"(%K IN %@)",propertyId, ids]];
    
    [fetchRequest setSortDescriptors:
     @[ [[NSSortDescriptor alloc] initWithKey: propertyId ascending:YES] ]];
    
    NSError *error;
    NSArray *objectsMatchingIds = [self.context executeFetchRequest:fetchRequest error:&error];
    return objectsMatchingIds;
}
@end
