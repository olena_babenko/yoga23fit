//
//  YFDataViewController.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YFStorage;
typedef enum{
    YFTableViewState = 1,
    YFCollectionViewState = 2
    
} YFViewState;
@interface YFDataViewController : UIViewController


@property (strong, nonatomic,readonly) YFStorage *storage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withStorage:(YFStorage *)storage;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
//OR
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
