//
//  YFAppDelegate.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAppDelegate.h"

#import "YFAsanasCatalogViewController.h"
#import "YFAsanasCollectionViewController.h"

#import "YFStorage.h"

@implementation YFAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    YFStorage *asanasStorage = [[YFStorage alloc] init];
    YFAsanasCollectionViewController *testController = [[YFAsanasCollectionViewController alloc] initWithNibName:@"YFAsanasCollectionViewController" bundle:nil withStorage:asanasStorage];
    //YFAsanasCatalogViewController *testController = [[YFAsanasCatalogViewController alloc] initWithNibName:@"YFAsanasCatalogViewController" bundle:nil withStorage:asanasStorage];
    
    [self.window setRootViewController:testController];
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
   // [self saveContext];
}





@end
