//
//  YFAsanasCatalogViewController.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAsanasCatalogViewController.h"
#import "YFDataViewController_Parent.h"

#import "YFAsana.h"
#import "YFFetchedResultTableDataSource.h"

#import "YFImportDataOperation.h"

#import "NSPredicate+Filters.h"

@interface YFAsanasCatalogViewController ()<YFFetchedResultsTableDataSourceDelegate>

@end

@implementation YFAsanasCatalogViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setStorage:(YFStorage *)storage
{
    [super setStorage:storage];
}
-(NSArray *)setValue:(id)value forKey:(NSString *)key forEach:(NSArray *)items
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:items.count];
    for (NSDictionary *item in items) {
        NSMutableDictionary *item_mut = [[NSMutableDictionary alloc] initWithDictionary:item];
        [item_mut setValue:value forKey:key];
        [array addObject:[item_mut copy]];
    }
    return [array copy];
}

-(NSString *)entityName
{
    return @"YFAsana";
}
-(NSArray *)sortDescriptors
{
    return @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
}

- (void)viewDidLoad
{
    self.state = YFTableViewState;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -  Search

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSPredicate* predicate = [NSPredicate asanaNumeNumberFilterForSearchText:searchText];
    [self.dataSource changePredicate:predicate];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

#pragma mark - FetchedResultDataSource


-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    YFAsana *itemAsana = (YFAsana *)item;
     UITableViewCell* cell = [aTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = itemAsana.name;
    return cell;
    
}

- (IBAction)tab_changed:(id)sender {
    
    UISegmentedControl *segment = (UISegmentedControl *)sender;


    
    NSDictionary *dictRoot;
    NSString *type_value;
    NSString *path_forResource;
    if (segment.selectedSegmentIndex == 0) {
        
         path_forResource = @"AsansType1";
         type_value = @"type 1";
    }
    
    if (segment.selectedSegmentIndex == 1) {
        
        path_forResource = @"AsansType2";
        type_value = @"type 2";
    }
    
//Все данные о чтении файлов должны управляться, например отдельным классом
//В этом классе должна быть проверка - заполнять ли базу данных данными из файлов(первый вход)
//Где то надо впихнуть операцию ПРОВЕРКИ которая справшивает о наличии обновлений
    
#pragma mark - BEGIN LOADING
    dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:path_forResource ofType:@"plist"]];
    BOOL is_loaded =  ((NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:path_forResource]).boolValue;
    
    
    if (!is_loaded) {
        
        NSArray *arrayList = [NSArray arrayWithArray:[dictRoot objectForKey:@"asanas"]];
        arrayList = [self setValue:type_value forKey:@"type" forEach:arrayList];
        YFImportDataOperation *import_op = [[YFImportDataOperation alloc] initWithStorage:self.storage entity:[self entityName] andData:arrayList];
        [self.operationQueue addOperation:import_op];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:path_forResource];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
#pragma mark -END LOADING
    
    self.dataFilter = [NSPredicate asanaTypeFilter:type_value];
    // Your dictionary contains an array of dictionary
    // Now pull an Array out of it.
   

}
@end
