//
//  YFGetDataOperation.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/27/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFGetDataOperation.h"
#import "YFStorage.h"
#import "YFParsing.h"

@interface YFGetDataOperation ()

@property (strong, nonatomic) YFStorage *storage;
@property (strong, nonatomic) NSString *entityName;
@property (strong, nonatomic) NSPredicate *predicate;

@end

@implementation YFGetDataOperation

-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andPredicate:(NSPredicate *)predicate
{
    self = [super init];
    if(self) {
        self.storage = storage;
        self.entityName = entityName;
        self.predicate = predicate;
    }
    return self;
} //get data using predicate
-(void)main
{
//1 get Main object Context (in Main Thread) to MANIPULATE objects easealy
    NSManagedObjectContext *context = [self.storage mainManagedObjectContext];
    context.undoManager = nil;
    
    [self makeRequestwithContext:context];
}
-(void)makeRequestwithContext:(NSManagedObjectContext *)context
{
//2 get Entitys id field name in DB
    Class<YFParsing> entity_class = NSClassFromString(self.entityName);
    NSString *propertyId = [entity_class propertyID];
    
    NSError* error = nil;
    NSMutableArray *results;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:_entityName];
//3 add the Predicate
    request.predicate = self.predicate;
//4 sort it by id
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:propertyId ascending:YES];
//5 execute
    results = [[context executeFetchRequest:request error:&error] copy];
    [results sortedArrayUsingDescriptors:@[sortDescriptor]];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
//6 return the results
    if (self.completeCallback) {
        self.completeCallback(results, error);
    }

}

@end
