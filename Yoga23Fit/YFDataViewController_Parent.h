//
//  YFDataViewController_Parent.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController.h"
#import "YFFetchedResultsDataSource.h"

@class YFStorage;
@interface YFDataViewController ()

@property (nonatomic, strong) NSOperationQueue* operationQueue;
@property (nonatomic, strong) NSPredicate *dataFilter;


@property (strong, nonatomic,readwrite) YFStorage *storage;
@property (assign, nonatomic) YFViewState state;

@property (strong, nonatomic) id<YFFetchedResultsDataSource> dataSource;

-(NSString *)entityName;
-(NSArray *)sortDescriptors;
-(NSString *)sectionNamePath;

-(NSInteger)secondsForUpdateForPath:(NSURL *)remotePath;

@end
