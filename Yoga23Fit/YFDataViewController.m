//
//  YFDataViewController.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController.h"
#import "YFStorage.h"
#import "YFDataViewController_Parent.h"


#import "YFFetchedResultTableDataSource.h"
#import "YFFetchedResultCollectionDataSource.h"


//privat interface
@interface YFDataViewController ()



@end

@implementation YFDataViewController

//DEFAULT VaLUE FOR UPDATE AFTER AN HOUR(can be more)
-(NSInteger)secondsForUpdateForPath:(NSURL *)remotePath
{
    return 3600;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withStorage:(YFStorage *)storage
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.storage = storage;
    }
    return self;
}
//create fetch result controller for Additioinal classes
-(NSString *)entityName
{
    return @"";
}
-(NSArray *)sortDescriptors
{
    return @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
}
-(NSString *)sectionNamePath
{
    return nil;
}

-(NSFetchedResultsController*)fetchResultController
{
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.sortDescriptors = [self sortDescriptors];
    fetchRequest.predicate = self.dataFilter;
    
    NSFetchedResultsController* fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.storage.mainManagedObjectContext sectionNameKeyPath:[self sectionNamePath] cacheName:nil];
    return fetchedResultsController;
}
//By default its Table View
-(void)setupDataSource
{
    switch (self.state) {
        case YFTableViewState:
        { self.dataSource = [[YFFetchedResultTableDataSource alloc] initWithView:self.tableView fetchedResultsController:[self fetchResultController]];
            self.tableView.dataSource = (YFFetchedResultTableDataSource *)self.dataSource;
            break;
        }
        case YFCollectionViewState:
        {
            self.dataSource = [[YFFetchedResultCollectionDataSource alloc] initWithView:self.collectionView fetchedResultsController:[self fetchResultController]];
            self.collectionView.dataSource = (YFFetchedResultCollectionDataSource *)self.dataSource;
        }
        default:
            break;
    }
    [self.dataSource setDelegate:self];
}

-(void)setDataFilter:(NSPredicate *)dataFilter
{
    _dataFilter = dataFilter;
    [self.dataSource changePredicate:dataFilter];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupDataSource];
    
	// Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.dataSource setPaused:NO];;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.dataSource setPaused:YES];
}

@end
