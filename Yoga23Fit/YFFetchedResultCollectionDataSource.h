//
//  YFFetchedResultCollectionDataSource.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YFFetchedResultsDataSource.h"

@protocol YFFetchedResultCollectionDataSourceDelegate <NSObject>

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item;
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath withInfo:(NSString *)info;

@end

@interface YFFetchedResultCollectionDataSource : NSObject<YFFetchedResultsDataSource, UICollectionViewDataSource>

@end
