//
//  YFActivity.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class YFActivityInProgram;

@interface YFActivity : NSManagedObject

@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * inner_folder_link;
@property (nonatomic, retain) NSNumber * is_complete;
@property (nonatomic, retain) NSNumber * is_short;
@property (nonatomic, retain) NSDate * last_update;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_en;
@property (nonatomic, retain) NSString * name_rus;
@property (nonatomic, retain) NSString * name_sun;
@property (nonatomic, retain) NSNumber * version;
@property (nonatomic, retain) NSManagedObject *inner_links;
@property (nonatomic, retain) NSSet *itemsInProgram;
@property (nonatomic, retain) NSManagedObject *outer_links;
@end

@interface YFActivity (CoreDataGeneratedAccessors)

- (void)addItemsInProgramObject:(YFActivityInProgram *)value;
- (void)removeItemsInProgramObject:(YFActivityInProgram *)value;
- (void)addItemsInProgram:(NSSet *)values;
- (void)removeItemsInProgram:(NSSet *)values;

@end
