//
//  YFAsana.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFActivity.h"
#import "YFParsing.h"

typedef enum{
    YFAsanaType1 = 1,
    YFAsanaType2 = 2,
    YFAsanaType3 = 3,
    YFAsanaTypeLast = 4,
    
} YFAsanaTypes;

@interface YFAsana : YFActivity<YFParsing>

@property (nonatomic, retain) NSNumber * catalogID;
@property (nonatomic, retain) NSString * family;
@property (nonatomic, retain) NSString * include_in_folders;
@property (nonatomic, retain) NSNumber * is_favorite;
@property (nonatomic, retain) NSNumber * level;
@property (nonatomic, retain) NSNumber * mirrored_id;
@property (nonatomic, retain) NSString * type;

-(YFAsanaTypes)currentType;

+(NSString *)stringFromType:(YFAsanaTypes) asanaType;
@end
