//
//  YFAsana.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFAsana.h"
#import "YFFileManager.h"

@implementation YFAsana

@dynamic catalogID;
@dynamic family;
@dynamic include_in_folders;
@dynamic is_favorite;
@dynamic level;
@dynamic mirrored_id;
@dynamic type;

+(NSString *)propertyID{
    return @"catalogID";
}
//get ID value from data, we should parse
+(id)idFromData:(id)data{
    NSAssert([data isKindOfClass:[NSDictionary class]], @"[YFAsana idFromData]: Data WRONG serializer (should be NSDictionary instead %@", [data class]);
    NSDictionary *parseDict = (NSDictionary *)data;
    NSString *trueId = parseDict[@"catalogID"];
   
    //NSAssert([trueId isKindOfClass:[NSString class]], @"[YFAsana idFromData]: wrong ID in %@", [parseDict description]);
    return trueId;
}
+(NSString *)version
{
    return @"version";
}

+(NSManagedObject *)createMOWithData:(id)data inContex:(NSManagedObjectContext *)context{
    
    NSString* entityName = NSStringFromClass(self);
    YFAsana* asana = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    [asana updateMOWithData:data];
    
    return asana;
}

-(void)updateMOWithData:(id)data{
    NSDictionary *parseDict = (NSDictionary *)data;
    self.catalogID = parseDict[@"catalogID"];
#warning TODO: Место для МАПИНГА
    self.type = parseDict[@"type"];
    self.name = parseDict[@"name"];
    self.version = parseDict[@"version"];
// check for update links!
    
// if we have outer links, without inner link we shoul create an folder
// create outer link if it does not Exist
    
}

-(YFAsanaTypes)currentType
{
    return [YFAsana typeFromString:self.type];
}


+(NSString *)stringFromType:(YFAsanaTypes) asanaType
{
    switch (asanaType) {
        case YFAsanaType1:
            return @"type 1";
        case YFAsanaType2:
            return @"type 2";
        case YFAsanaType3:
            return @"type 3";
        default:
            break;
    }
    return nil;
    
}

+(YFAsanaTypes)typeFromString:(NSString *)stringType
{
    for (int i = 0; i< YFAsanaTypeLast; i++) {
        if ([stringType isEqualToString:[YFAsana stringFromType:i]]) {
            return (YFAsanaTypes)i;
        }
    }
    return 0;
}


@end
