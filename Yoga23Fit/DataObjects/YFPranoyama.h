//
//  YFPranoyama.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFActivity.h"


@interface YFPranoyama : YFActivity

@property (nonatomic, retain) NSNumber * duration_in_sec;

@end
