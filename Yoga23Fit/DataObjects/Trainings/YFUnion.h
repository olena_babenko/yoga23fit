//
//  YFUnion.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFExecutable.h"

@class YFExecutable;

@interface YFUnion : YFExecutable

@property (nonatomic, retain) NSSet *subItems;
@property (nonatomic, retain) YFExecutable *currentItem;

@end

@interface YFUnion (CoreDataGeneratedAccessors)

- (void)addSubItemsObject:(YFExecutable *)value;
- (void)removeSubItemsObject:(YFExecutable *)value;
- (void)addSubItems:(NSSet *)values;
- (void)removeSubItems:(NSSet *)values;

@end
