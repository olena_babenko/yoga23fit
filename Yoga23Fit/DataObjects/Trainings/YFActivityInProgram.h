//
//  YFActivityInProgram.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFExecutable.h"

@class YFActivity;

@interface YFActivityInProgram : YFExecutable

@property (nonatomic, retain) NSNumber * fix_time_in_sec;
@property (nonatomic, retain) NSNumber * repetitionCount;
@property (nonatomic, retain) YFActivity *activityInfo;

@end
