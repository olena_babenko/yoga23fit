//
//  YFSourceLinks.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFSourceLinks.h"
#import "YFActivity.h"


@implementation YFSourceLinks

@dynamic additional_photos;
@dynamic icon_png;
@dynamic icon_svg;
@dynamic photo_png;
@dynamic photo_scheme_png;
@dynamic video;
@dynamic owner_local;
@dynamic owner_remote;


+(NSString *)propertyID
{
    return  nil;
}
+(id)idFromData:(id)data
{
    return nil;
}
+(NSString *)version
{
    return nil;
}

+(NSManagedObject *)createMOWithData:(id)data inContex:(NSManagedObjectContext *)context
{
    NSString* entityName = NSStringFromClass(self);
    YFSourceLinks * source = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    [source updateMOWithData:data];
    
    return source;
}
-(void)updateMOWithData:(id)data
{
    
}
@end
