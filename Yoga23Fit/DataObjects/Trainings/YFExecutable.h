//
//  YFExecutable.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class YFUnion;

//table executable include Groups and Activities

@interface YFExecutable : NSManagedObject

@property (nonatomic, retain) NSString * breath_type;
@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSNumber * duration_in_sec;
@property (nonatomic, retain) NSNumber * indexInSuper;
@property (nonatomic, retain) YFUnion *superItem;

//команда вызывает на выполнение асану или программу
-(void)run;

@end
