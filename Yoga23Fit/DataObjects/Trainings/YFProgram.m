//
//  YFProgram.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFProgram.h"
#import "YFExecutable.h"


@implementation YFProgram

@dynamic info;
@dynamic inner_folder_link;
@dynamic is_complete;
@dynamic is_short;
@dynamic name;
@dynamic preferable_time;
@dynamic recommendation;
@dynamic version;
@dynamic subItems;


-(void)run
{
    //[super run];
}
@end
