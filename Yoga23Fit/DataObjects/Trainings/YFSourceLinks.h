//
//  YFSourceLinks.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFParsing.h"

@class YFActivity;

@interface YFSourceLinks : NSManagedObject<YFParsing>

@property (nonatomic, retain) NSString * additional_photos;
@property (nonatomic, retain) NSString * icon_png;
@property (nonatomic, retain) NSString * icon_svg;
@property (nonatomic, retain) NSString * photo_png;
@property (nonatomic, retain) NSString * photo_scheme_png;
@property (nonatomic, retain) NSString * video;
@property (nonatomic, retain) YFActivity *owner_local;
@property (nonatomic, retain) YFActivity *owner_remote;

@end
