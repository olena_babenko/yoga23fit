//
//  YFProgram.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "YFUnion.h"

@class YFExecutable;

@interface YFProgram : YFUnion

@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * inner_folder_link;
@property (nonatomic, retain) NSNumber * is_complete;
@property (nonatomic, retain) NSNumber * is_short;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * preferable_time;
@property (nonatomic, retain) NSString * recommendation;
@property (nonatomic, retain) NSNumber * version;
@property (nonatomic, retain) NSSet *subItems;
@end

@interface YFProgram (CoreDataGeneratedAccessors)

- (void)addSubItemsObject:(YFExecutable *)value;
- (void)removeSubItemsObject:(YFExecutable *)value;
- (void)addSubItems:(NSSet *)values;
- (void)removeSubItems:(NSSet *)values;

@end
