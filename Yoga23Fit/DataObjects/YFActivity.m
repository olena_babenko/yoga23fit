//
//  YFActivity.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFActivity.h"
#import "YFActivityInProgram.h"


@implementation YFActivity

@dynamic info;
@dynamic inner_folder_link;
@dynamic is_complete;
@dynamic is_short;
@dynamic last_update;
@dynamic name;
@dynamic name_en;
@dynamic name_rus;
@dynamic name_sun;
@dynamic version;
@dynamic inner_links;
@dynamic itemsInProgram;
@dynamic outer_links;

@end
