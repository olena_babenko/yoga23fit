//
//  NSArray+checngeValueInAllObjects.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "NSArray+checngeValueInAllObjects.h"

@implementation NSArray (checngeValueInAllObjects)


-(NSArray *)foreachSetValue:(id)value forKey:(NSString *)key
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:self.count];
    for (NSDictionary *item in self) {
        NSMutableDictionary *item_mut = [[NSMutableDictionary alloc] initWithDictionary:item];
        [item_mut setValue:value forKey:key];
        [array addObject:[item_mut copy]];
    }
    return [array copy];
}
@end
