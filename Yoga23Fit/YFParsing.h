//
//  YFParsing.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/26/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YFParsing <NSObject>

//Class for Parsing server object to Managed Object

//unique property name, that can be used as ID
+(NSString *)propertyID;
//get ID value from data, we should parse
+(id)idFromData:(id)data;
//get Version from data, we should parse
+(NSString *)version;

+(NSManagedObject *)createMOWithData:(id)data inContex:(NSManagedObjectContext *)context;
-(void)updateMOWithData:(id)data;



@end
