//
//  YFFetchedResultsDataSource.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YFFetchedResultsDataSource <NSObject>

- (id)initWithView:(UIScrollView*)aView fetchedResultsController:(NSFetchedResultsController*)aFetchedResultsController;
- (void)changePredicate:(NSPredicate*)predicate;

- (void)setPaused:(BOOL)paused;
- (void)setDelegate:(id)delegate;

-(BOOL)isEmpty;

@end
