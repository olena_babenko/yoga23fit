//
//  YFFileManager.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/6/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YFFileManager : NSObject

//return folder or create
+(NSURL *)pathForFolderCatalogObject:(NSString *)folderName;
+(NSURL *)pathForFolderProgramObject:(NSString *)folderName;

+(BOOL)isExistFileOrFolder:(NSURL *)url;



+(BOOL)removeFileOrFolderFromURL:(NSURL *)url;
@end
