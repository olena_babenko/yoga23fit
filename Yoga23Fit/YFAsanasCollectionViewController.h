//
//  YFAsanasCollectionViewController.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/4/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFDataViewController.h"



@interface YFAsanasCollectionViewController : YFDataViewController<UICollectionViewDelegate, UISearchBarDelegate>

- (IBAction)tab_changed:(id)sender;

@end
