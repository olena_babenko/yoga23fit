//
//  YFFetchedResultTableDataSource.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/3/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YFFetchedResultsDataSource.h"

@protocol YFFetchedResultsTableDataSourceDelegate <NSObject>

-(UITableViewCell *)tableView:(UITableView*)aTableView cellForRowAtIndexPath:(NSIndexPath*)indexPath withItem:(id) item;

@end
@interface YFFetchedResultTableDataSource : NSObject<YFFetchedResultsDataSource,UITableViewDataSource>


@end
