//
//  YFVersionsCheckOperation.m
//  Yoga23Fit
//
//  Created by Olena Babenko on 3/5/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "YFVersionsCheckOperation.h"
#import "YFStorage.h"
#import "YFParsing.h"

#import "NSPredicate+Filters.h"

#define version_serverField @"version"
#define id_serverField @"id"

@interface YFVersionsCheckOperation ()

@property (strong, nonatomic) NSURL *updateURL;
@property (strong, nonatomic) YFStorage *storage;
@property (strong, nonatomic) NSString *entityName;
@property (nonatomic, strong) NSManagedObjectContext* context;

@end

@implementation YFVersionsCheckOperation

-(id)initWithStorage:(YFStorage *)storage entity:(NSString *)entityName andUpdateURL:(NSURL *)updateURL
{
    self = [super init];
    if (self) {
        self.storage = storage;
        self.entityName = entityName;
        self.updateURL = updateURL;
    }
    return self;
}

-(void)main
{
    self.context = [self.storage privateContext];
    self.context.undoManager = nil;
    [self.context performBlockAndWait:^(void)
     {
         [self check];
     }];
    
}

-(void)check
{
//1 find Arrays of Id + Versions
    NSArray *versionsArray = [self versionsArray];
    //NSLog(@"versions Array %@", versionsArray);
    if (!self.isCancelled && versionsArray.count>0) {
        
//2 lets make an request to get Array
        NSURLRequest *request = [self versionRequestWithArray:versionsArray];
        
//3 send an request and got an answer
        NSURLResponse *response;
        NSError *error = nil;
        NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (!self.isCancelled && result) {
         
            NSArray *objectsToUpdate;
            if (error.code == noErr) {
                error = nil;
                objectsToUpdate= [NSJSONSerialization JSONObjectWithData:result options:0 error:&error];
            }
            
            if (self.completeCallback) {
                self.completeCallback(objectsToUpdate, error);
            }
        }
    }
    
}
-(NSArray *)versionsArray
{
    NSMutableArray *versionsArray = [[NSMutableArray alloc] init];
    
    Class<YFParsing> entity_class = NSClassFromString(self.entityName);
    NSString *propertyId = [entity_class propertyID];
    NSString *version = [entity_class version];
    
//Lets find All Entities
    NSError* error = nil;
    NSMutableArray *results;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:_entityName];
    results = [[_context executeFetchRequest:request error:&error] copy];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        if (self.completeCallback) {
            self.completeCallback(nil, error);
        }
        return nil;
    }
//Lets make an Array with Id's and Versions Dictionaries
/*
[
 { id = 23;
   version = 3;
 },
 {
   id = 45;
   version = 1;
 }
]
*/
//TODO: If there is any changes in JSON - CHANGE IT Here
    NSArray *allIds = [results valueForKey:propertyId];
    NSArray *allVersions = [results valueForKey:version];
    if (allIds.count != allVersions.count) {
        NSAssert(NO, @"[YFVersionsCheckOperation versionsArray]: Ids array count %d != VersionId array %d", allIds.count, allVersions.count);
    }
    //
    [allIds enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    
        if (self.isCancelled) {
            *stop = YES;
            return;
        }
        NSDictionary *one_object = @{id_serverField:allIds[idx], version_serverField:allVersions[idx]};
        [versionsArray addObject:one_object];
        
    }];
    return versionsArray;
    
}

-(NSURLRequest *)versionRequestWithArray:(NSArray *)versionsArray
{
    NSURLRequest *request = [NSURLRequest requestWithURL:self.updateURL];
//TODO: wright exactly body!
    return request;
}
@end
