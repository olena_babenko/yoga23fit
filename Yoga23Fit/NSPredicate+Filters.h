//
//  NSPredicate+Filters.h
//  Yoga23Fit
//
//  Created by Olena Babenko on 2/25/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPredicate (Filters)


+(NSPredicate *)activityNameInclude:(NSString *)name_search;
+(NSPredicate *)programNameInclude:(NSString *)name_search;

+(NSPredicate *)asanaTypeFilter:(NSString *)typeName;
+(NSPredicate *)asanaFamilyFilter:(NSString *)familyName;
+(NSPredicate *)asanaNameIncludeFilter:(NSString *)name;
+(NSPredicate *)asanaNumberFilter:(NSNumber *)number;
+(NSPredicate *)asanaNumeNumberFilterForSearchText:(NSString*) searchText;


@end
